
function randomResult(){
     var result;

}

randomResult.prototype.setValues=(function(values){
     this.result = values;
});

randomResult.prototype.getValues = (function(){
     return(this.result);
});

function randomdotorgRequest() {

}

randomdotorgRequest.prototype.randomInt = (function(data, f){
     //data = {min:int, max:int, num:int}

     data.format = "plain";
     data.col = 1;
     data.base = 10;
     data.rnd = 'new';

     try {

     $.ajax("http://www.random.org/quota/", {type:'get',dataType:'text','data':{format:'plain'}, timeout:120000,  success: function(result){
          result = parseInt(result);

          if(result > 0){
               $.ajax("http://www.random.org/integers/", {type:'get',dataType:'text','data':data, timeout:120000,  success: function(result){

                    var intArray = result.split("\n");

                    $.each(intArray, function(index, value){
                         if(value !== ""){
                              intArray[index] = parseInt(value);
                         }
                         else {
                              intArray.splice(index, 1);
                         }
                    });

                    var answer = new randomResult
                    answer.setValues(intArray);

                    f(answer);

               }, error: this.displayError});
          }
          else {
               throw "Out of requests";
          }

     }
     });
     }
     catch(err){
          console.log(err);
     }
});

randomdotorgRequest.prototype.displayError = (function(message){

     console.log(message);

});


randomdotorgRequest.prototype.randomSequence = (function(data, f){
     data.format = "plain";
     data.col = 1;
     data.rnd = 'new';

     try {

     $.ajax("http://www.random.org/quota/", {type:'get',dataType:'text','data':{format:'plain'}, timeout:120000,  success: function(result){
          result = parseInt(result);

          if(result > 0){
               $.ajax("http://www.random.org/sequences/", {type:'get',dataType:'text','data':data, timeout:120000,  success: function(result){

                    var intArray = result.split("\n");

                    $.each(intArray, function(index, value){
                         if(value !== ""){
                              intArray[index] = parseInt(value);
                         }
                         else {
                              intArray.splice(index, 1);
                         }
                    });

                    var answer = new randomResult
                    answer.setValues(intArray);

                    f(answer);

               }, error: this.displayError});
          } else {
               throw "Out of requests";
          }
     }});
     }
     catch (err){
          console.log(err);
     }
});

randomdotorgRequest.prototype.randomString = (function(data, f){
     data.format = "plain";
     data.col = 1;
     data.rnd = 'new';

     if(typeof(data.digits) === "undefined"){
          data.digits = 'on';
     }
     else if(data.digits !== 'off' || data.digits !== false) {
          data.digits = 'on';
     }
     else {
          data.digits = 'off';
     }

     if(typeof(data.upperalpha) === "undefined"){
          data.upperalpha = 'on';
     }
     else if(data.upperalpha !== 'off' || data.upperalpha !== false) {
          data.upperalpha = 'on';
     }
     else {
          data.upperalpha = 'off';
     }

     if(typeof(data.loweralpha) === "undefined"){
          data.loweralpha = 'on';
     }
     else if(data.loweralpha !== 'off' || data.loweralpha !== false) {
          data.loweralpha = 'on';
     }
     else {
          data.loweralpha = 'off';
     }

     if(typeof(data.unique) === "undefined"){
          data.unique = 'on';
     }
     else if(data.unique !== 'off' || data.unique !== false) {
          data.unique = 'on';
     }
     else {
          data.unique = 'off';
     }

     $.ajax("http://www.random.org/strings/", {type:'get',dataType:'text','data':data, timeout:120000,  success: function(result){

          var strArray = result.split("\n");

          $.each(strArray, function(index, value){
               if(value !== ""){
                    strArray[index] = value;
               }
               else {
                    strArray.splice(index, 1);
               }
          });

          var answer = new randomResult
          answer.setValues(strArray);

          f(answer);

     }, error: this.displayError});

});

randomdotorgRequest.prototype.checkQuota = (function(f){

     data = {format: 'plain'};

     $.ajax("http://www.random.org/quota/", {type:'get',dataType:'text','data':data, timeout:120000,  success: function(result){

          var answer = new randomResult
          answer.setValues(parseInt(result));
          f(answer);

     }, error: this.displayError});

});


randomdotorgRequest.prototype.randomize = (function(valueArray, f){
     var len = valueArray.length;
     var newValueArray = [];


     var randomizeArray = (function(randomIndexes){
          var indexes = randomIndexes.getValues();
          for(var i = 0; i<len; i++){
               newValueArray[indexes[i]] = valueArray[i];
          }

          var result = new randomResult;
          result.setValues(newValueArray);
          f(result);

     });

     this.randomSequence({min:0, max:len-1}, randomizeArray);

});

window.randomdotorg = new randomdotorgRequest();