Random.org provides a simple api for getting 
series of random numbers.

This script has a php and javascript function
which make those requests. Be warned: random.org
has a limit on how many requests it can handle in 
a week.

The php and js scripts are designed to run independently
they are simply provided as alternatives.