<?php

     class randomdotorg {
          CONST RANDOMIZERURL = "http://www.random.org/";
          CONST RANDOMIZEROPTS = "cols=1&format=plain&rnd=new"

          private function appendData(&$url, $data){
               $i = 0;
               $max = count($data);
               foreach($data as $key=>$val){
                    $url .= $key."="$val;
                    $i++;
                    if($i < $max){
                         $url.="&";
                    }
               }
          }

          private function sizeofvar($var){
               $start_memory = memory_get_usage();
               $tmp = unserialize(serialize($var));
               return memory_get_usage() - $start_memory;
          }

          public function randomIntegers($data){

               if($this->getQuota() >= $this->sizeofvar($data['max']*$data['num'])){

                    $url = self::RANDOMIZERURL."integers/";

                    $url.= "?base=10&".self::RANDOMIZEROPTS;

                    $this->appendData($url, $data);

                    $resultString = file_get_contents($url);
                    $result = explode("\n", $resultString);
                    foreach($result as $index=>$value){
                         if($value !== ""){
                              $result[$index]= intVal($value);
                         }
                         else {
                              array_splice($result,$index,1);
                         }
                    }

                    $result = array_values($result);

                    return($result);
               }


          }

          public function randomSequence($data){

               if($this->getQuota() >= $this->sizeofvar(pow($data['max']-$data['min'],2))){

                    $url = self::RANDOMIZERURL."sequences/";

                    $url .= "?".self::RANDOMIZEROPTS;

                    $this->appendData($url, $data);
                    $resultString = file_get_contents($url);
                    $result = explode("\n", $resultString);
                    foreach($result as $index=>$value){
                         if($value !== ""){
                              $result[$index]= intVal($value);
                         }
                         else {
                              array_splice($result,$index,1);
                         }
                    }

                    $result = array_values($result);

                    return($result);
               }

          }

          public function randomStrings($data){

               if($this->getQuota() >= $this->sizeofvar($data['num']*$data['len'])){
                    $url = self::RANDOMIZERURL."strings/";

                    $url.= "?".self::RANDOMIZEROPTS;

                    $opts = clone $data;

                    $opts["digits"] = (!empty($opts['digits'])) ? $opts['digits'] : "on";
                    $opts["upperalpha"] = (!empty($opts['upperalpha'])) ? $opts['upperalpha'] : "on";
                    $opts["loweralpha"] = (!empty($opts['loweralpha'])) ? $opts['loweralpha'] : "on";

                    $this->appendData($url, $opts);

                    $resultString = file_get_contents($url);
                    $result = explode("\n", $resultString);
                    foreach($result as $index=>$value){
                         if($value !== ""){
                              $result[$index]= intVal($value);
                         }
                         else {
                              array_splice($result,$index,1);
                         }
                    }

                    $result = array_values($result);

                    return($result);
               }
          }

          public function getQuota(){
               $url = self::RANDOMIZERURL."quota/";

               $url .= "?format=plain";

               $resultString = file_get_contents($url);
               $bits = intVal($resultString);

               return($bits);
          }

          public function randomize($values){

               $len = count($values);

               $randomIndexes = $this->randomSequence(['min'=>0, 'max'=>len-1]);

               $randomValues = [];
               for($i=0;$i<$len;$i++){
                    $randomValues[$randomIndexes[$i]] = $values[$i];
               }

               return($randomValues);
          }
     }
